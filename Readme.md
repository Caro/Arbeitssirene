# Arbeitssirene und Kaffeezähler
## Beschreibung
Das Gerät soll den Beginn und das Ende der Pausen, akustisch mit einer Sirene und optisch mit vier bunten Lampen, anzeigen.

Des Weiteren soll die Möglichkeit bestehen, dass der Kaffeeverbrauch jeder Person im Büro getrackt wird. Pro Person soll ein entsprechender Pushbutton vorhanden sein.

Die Uhr soll sich Netzwerkunabhängig die Zeit besorgen.

## Ausführungen
- Rasperry Pi
- E-Ink Display für ca. 5 Zeilen
- 5 Push-Buttons (1 Pro Person + 1 Extra)
- Netzwerkunabhängige Zeiterkennung
- Sirene(ngeräusch)
- 4 farbige LED (Ausführung als Bulb)
- Programmiert in C#

## ToDo
- [ ] Rausfinden, wie man den Pi mit C# programmiert
- [ ] Ansteuerung CPU und Display
- [ ] Zeitsynchronisierung
  - [ ] DCF77
  - [ ] GPS oder Gallileo
  - [ ] RTC (Echtzeituhr)
- [ ] Platinen entwerfen
  - [ ] für fünf Buttons
  - [ ] für vier LED
  - [ ] Sirene(nsound)
- [ ] Kaffeezähler pro Person (kann zurück gesetzt werden)
- [ ] Gehäuse designen (3D Druckbar)